/*
 * MainAdapterPettern.java
 *
 * created at Dec 11, 2019 by d.balamdzhiev <d.balamdzhiev@seeburger.com>
 *
 * Copyright (c) SEEBURGER AG, Germany. All Rights Reserved.
 */
package com.doncho.adapter.pattern;


public class MainAdapterPettern
{

    public static void main(String[] args)
    {
        System.out.println("Adapter is here!");

        IAdapterConverter converter = new XMLIncomeFile();
        IXMLtoJSONAdapter xmlToJsonAdapter = new XMLtoJsonImpl(converter);

        System.out.println("***** " + xmlToJsonAdapter.convertFile() + " *****");
        System.out.println("Adapter is done!");
    }

}



