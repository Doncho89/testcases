/*
 * XMLtoJsonImpl.java
 *
 * created at Dec 11, 2019 by d.balamdzhiev <d.balamdzhiev@seeburger.com>
 *
 * Copyright (c) SEEBURGER AG, Germany. All Rights Reserved.
 */
package com.doncho.adapter.pattern;


public class XMLtoJsonImpl implements IXMLtoJSONAdapter
{
    private IAdapterConverter converter;


    public XMLtoJsonImpl(IAdapterConverter converter)
    {
        super();
        this.converter = converter;
    }


    @Override
    public String convertFile()
    {
        return convertXMLToJson(converter.convertFile());
    }


    private String convertXMLToJson(String value)
    {
        return value + " and I am converted to JSON file!";
    }

}



