/*
 * IAdapterConverter.java
 *
 * created at Dec 11, 2019 by d.balamdzhiev <d.balamdzhiev@seeburger.com>
 *
 * Copyright (c) SEEBURGER AG, Germany. All Rights Reserved.
 */
package com.doncho.adapter.pattern;


public interface IAdapterConverter
{
    public String convertFile();
}



