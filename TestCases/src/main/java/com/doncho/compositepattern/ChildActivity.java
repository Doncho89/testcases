/*
 * ChildActivity.java
 *
 * created at Feb 27, 2019 by d.balamdzhiev <d.balamdzhiev@seeburger.com>
 *
 * Copyright (c) SEEBURGER AG, Germany. All Rights Reserved.
 */
package com.doncho.compositepattern;

import java.util.Stack;

import javax.xml.bind.annotation.XmlAttribute;

public class ChildActivity implements IActivity
{

    @XmlAttribute(name="name")
    private String name;
    @XmlAttribute(name="ref")
    private String ref;


    public ChildActivity()
    {
     // Default
    }

    public ChildActivity(String name, String ref)
    {
        super();
        this.name = name;
        this.ref = ref;
    }


    @Override
    public String getName()
    {

        return name;
    }


    @Override
    public void remove(ChildActivity child)
    {
        // This is leaf it can't add item.

    }


    @Override
    public void print()
    {
        System.out.println("\tChild id " + getID() + " child name: " + getName());

    }


    @Override
    public String getID()
    {
        return name;
    }


    @Override
    public void add(IActivity child1)
    {
     // This is leaf it can't add item.

    }

	@Override
	public IActivity getLastAddedElement() {
		// This is leaf it can't add item.
		return null;
	}

	@Override
	public IActivity getChild(int i) {
		// This is child
		return null;
	}

	@Override
	public Stack<IActivity> getChilds() {
		// This is child
		return null;
	}

}
