/*
 * IActivity.java
 *
 * created at Feb 27, 2019 by d.balamdzhiev <d.balamdzhiev@seeburger.com>
 *
 * Copyright (c) SEEBURGER AG, Germany. All Rights Reserved.
 */
package com.doncho.compositepattern;

import java.util.Stack;

public interface IActivity
{

    public String getName();
    public String getID();
    public IActivity getChild(int i);
    public Stack<IActivity> getChilds();
    public void add(IActivity child);
    public void remove(ChildActivity child);
    public void print();
    public IActivity getLastAddedElement();

}



