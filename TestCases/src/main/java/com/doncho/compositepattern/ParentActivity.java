/*
 * ParentActivity.java
 *
 * created at Feb 27, 2019 by d.balamdzhiev <d.balamdzhiev@seeburger.com>
 *
 * Copyright (c) SEEBURGER AG, Germany. All Rights Reserved.
 */
package com.doncho.compositepattern;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement(name="process")
public class ParentActivity implements IActivity
{
    @XmlAttribute(name="name")
    private String name;
    @XmlAttribute(name="ref")
    private String ref;
    @XmlElements  ({@XmlElement(name = "node", required = true, nillable = true, type = ParentActivity.class),
    @XmlElement(name = "node", required = true, nillable = true, type = ChildActivity.class)})
    //private List<IActivity> childs = new ArrayList<IActivity>();
    Stack<IActivity> childs = new Stack<IActivity>();

    public ParentActivity()
    {
        // Default
    }

    public ParentActivity(String name, String ref)
    {
        this.name = name;
        this.ref = ref;
    }

    @Override
    public String getName()
    {
        // TODO Auto-generated method stub
        return name;
    }


    @Override
    public void remove(ChildActivity child)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void print()
    {
        System.out.println("Parent id " + getID() + " parent name: " + getName());

        for (int i = 0; i < childs.size(); i++)
        {
                //System.out.println("Childs: ");
                childs.get(i).print();
        }

    }

    @Override
    public String getID()
    {
        // TODO Auto-generated method stub
        return name;
    }

    @Override
    public void add(IActivity child)
    {
        childs.add(child);

    }

	@Override
	public IActivity getLastAddedElement() {
		
		//int childsSize = childs.size();
		if(childs.isEmpty())
		{
			return null;
		}
//		if(childsSize == 0)
//		{
//			return childs.get(childsSize);
//		}
		return childs.peek();
		
	}

	@Override
	public IActivity getChild(int i) {
	
		return childs.get(i);
	}

	@Override
	public Stack<IActivity> getChilds() {
		
		Stack<IActivity> resultList = new Stack<IActivity>();
		for(int i =0; i<childs.size(); i++)
		{
			resultList.add(childs.get(i));
		}
		
		return resultList;
	}

}



