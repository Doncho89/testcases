/*
 * Main.java
 *
 * created at Feb 27, 2019 by d.balamdzhiev <d.balamdzhiev@seeburger.com>
 *
 * Copyright (c) SEEBURGER AG, Germany. All Rights Reserved.
 */
package com.doncho.compositepattern;

import java.io.File;
import java.util.Scanner;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class Main {

	private static IActivity parent;

	public static void main(String[] args) {
		File file = new File(System.getProperty("user.home")+ "\\Desktop\\compositeJAXB.xml");

		IActivity child1 = new ChildActivity("Copy", "process/se[1]/rc[1]");
		IActivity child2 = new ChildActivity("newFile", "process/se[1]/rc[1]");
		IActivity child3 = new ChildActivity("Asign", "process/se[1]/rc[1]");
		IActivity child4 = new ChildActivity("apendString", "process/se[1]/rc[1]");
		IActivity child5 = new ChildActivity("Asign", "process/se[1]/rc[1]");

		IActivity parentNull = new ParentActivity("Scope", "process/se[1]/rc[1]");
		parentNull.add(child5);

		IActivity parent1 = new ParentActivity("Scope", "process/se[1]/rc[1]");
		parent1.add(child1);
		parent1.add(child2);

		IActivity parent2 = new ParentActivity("Scope", "process/se[1]/rc[1]");
		parent2.add(child3);
		parent2.add(parent1);

		// parent = constructParent();

		IActivity parent3 =  new ParentActivity("testSystemVariables", "process");
        parent3.add(parent1);
        parent3.add(parent2);
        parent3.add(child4);
        parent3.add(child5);

		//parent3.print();

		ExecutionHistoryElement ex = new ExecutionHistoryElement(parent3);
		ProcessNodeRoot root = new ProcessNodeRoot("250511c0-35b6-11e9-936e-5e930a0a3094",
				"{http://www.seeburger.com/}testSystemVariables", "5", ex);

		try {
			marshallTree(file, root);
		} catch (JAXBException e) {
			e.printStackTrace();
		}

	}

	private final static void marshallTree(File file, ProcessNodeRoot parent) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(ProcessNodeRoot.class);
		Marshaller marschall = context.createMarshaller();

		marschall.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		marschall.marshal(parent, System.out);
		marschall.marshal(parent, file);
	}

	private final static IActivity constructParent() {
		Scanner scan = new Scanner(System.in);
		System.out.print("name");
		String name = scan.nextLine();
		System.out.print("path");
		String path = scan.nextLine();

		parent = new ParentActivity(name, path);

		boolean counter = true;

		while (counter) {
			System.out.print("parent = 0;  child = 1 ; set 2 to exit");
			String decide = scan.nextLine();

			if (decide.equals("2")) {
				counter = false;
				break;
			}

				System.out.print("name");
				name = scan.nextLine();
				System.out.print("path");
				path = scan.nextLine();

				if(decide.equals("0"))
				{
					if(parent.getChilds().isEmpty())
					{
						parent.add(new ParentActivity(name, path));
					}
					else
					{
						//parent.getChild(0).add(new ParentActivity(name, path));
						parent.getLastAddedElement().add(new ParentActivity(name, path));
					}
				}
				else if(decide.equals("1"))
				{
				  parent.getLastAddedElement().add(new ChildActivity(name, path));
				}

			}

		return parent;
	}

	private static void initParrent(String name, String path) {
		if (parent == null) {
			parent = new ParentActivity(name, path);
		} else {
			parent = parent.getLastAddedElement();
		}
	}

}
