/*
 * ProcessNodeRoot.java
 *
 * created at Feb 27, 2019 by d.balamdzhiev <d.balamdzhiev@seeburger.com>
 *
 * Copyright (c) SEEBURGER AG, Germany. All Rights Reserved.
 */
package com.doncho.compositepattern;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="process")
public class ProcessNodeRoot
{
    @XmlAttribute(name="id")
    private String id;

    @XmlAttribute(name="name")
    private String name;

    @XmlAttribute(name="state")
    private String state;

    @XmlElement(name="execution-history")
    ExecutionHistoryElement executionHistory;

    public ProcessNodeRoot()
    {
        // Default
    }

    public ProcessNodeRoot(String id, String name, String state, ExecutionHistoryElement executionHistory)
    {
        this.id = id;
        this.name = name;
        this.state = state;
        this.executionHistory = executionHistory;
    }

}



