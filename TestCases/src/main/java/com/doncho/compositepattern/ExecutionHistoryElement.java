/*
 * ExecutionHistoryElement.java
 *
 * created at Feb 27, 2019 by d.balamdzhiev <d.balamdzhiev@seeburger.com>
 *
 * Copyright (c) SEEBURGER AG, Germany. All Rights Reserved.
 */
package com.doncho.compositepattern;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;

public class ExecutionHistoryElement
{
    @XmlElements  ({@XmlElement(name = "node", required = true, nillable = true, type = ParentActivity.class),
    @XmlElement(name = "node", required = true, nillable = true, type = ChildActivity.class)})
    private IActivity activity;

    public ExecutionHistoryElement()
    {
        // Default
    }

    public ExecutionHistoryElement(IActivity activity)
    {
        this.activity = activity;
    }
}



