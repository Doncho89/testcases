/*
 * MyJaxbRootModel.java
 *
 * created at Nov 22, 2019 by d.balamdzhiev <d.balamdzhiev@seeburger.com>
 *
 * Copyright (c) SEEBURGER AG, Germany. All Rights Reserved.
 */
package com.doncho.jaxb.casus;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name="rootMap")
public class MyJaxbRootModel
{
    //@XmlElement(name="propertyMap")
    @XmlElements({@XmlElement(name = "property", type = LinkedWrapperClass.class),
                 @XmlElement(name = "property", type = LinkedWrapperWrapperClass.class)})
    private List<AbstractLinkedWrapper> myProps = null;

    public MyJaxbRootModel()
    {
        // TODO Auto-generated constructor stub
    }

    public MyJaxbRootModel(List<AbstractLinkedWrapper> myMap) {
        this.myProps = myMap;
    }

    @XmlTransient
    public List<AbstractLinkedWrapper> getMyMap() {
        return myProps;
    }

    public void setMyMap(List<AbstractLinkedWrapper> myMap) {
        this.myProps = myMap;
    }

}



