/*
 * LinkedWrapperClass.java
 *
 * created at Nov 22, 2019 by d.balamdzhiev <d.balamdzhiev@seeburger.com>
 *
 * Copyright (c) SEEBURGER AG, Germany. All Rights Reserved.
 */
package com.doncho.jaxb.casus;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

public class LinkedWrapperClass extends AbstractLinkedWrapper
{

    @XmlAttribute(name="key")
    private String key;

    @XmlAttribute(name="value")
    private String value;

    public LinkedWrapperClass() {
        // TODO Auto-generated constructor stub
    }



    public LinkedWrapperClass(String key, String value) {
        this.key = key;
        this.value = value;
    }


    @XmlTransient
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @XmlTransient
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}



