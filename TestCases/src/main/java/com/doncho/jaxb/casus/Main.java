/*
 * Main.java
 *
 * created at Nov 22, 2019 by d.balamdzhiev <d.balamdzhiev@seeburger.com>
 *
 * Copyright (c) SEEBURGER AG, Germany. All Rights Reserved.
 */
package com.doncho.jaxb.casus;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class Main
{
    public static void main(String[] args) throws JAXBException {

        // Initialize maps
        Map<String, Object> map1 = new LinkedHashMap<String, Object>();
        Map<String, Object> map2 = new LinkedHashMap<String, Object>();
        Map<String, Object> map3 = new LinkedHashMap<String, Object>();
        Map<String, Object> map4 = new LinkedHashMap<String, Object>();
        Map<String, Object> map5 = new LinkedHashMap<String, Object>();
        Map<String, Object> map6 = new LinkedHashMap<String, Object>();

        //Fill maps with values.
        fillMaps(map1);
        fillMaps(map2);
        fillMapsInner(map3, map4);
        fillMapsInner(map5, map6);

        // Print maps values for better view.
        System.out.println(map1);
        System.out.println(map2);
        System.out.println(map3);
        System.out.println(map4);

        // Fill base map with already constructed test maps.
        Map<String, Object> myMapDoncho = new LinkedHashMap<String, Object>();
        myMapDoncho.put("ip", "564544646");
        myMapDoncho.put("attachment", map1);
        myMapDoncho.put("message", map2);
        myMapDoncho.put("message1", map3);
        myMapDoncho.put("inner", map5);

        System.out.println(myMapDoncho);

        MyJaxbRootModel model = new MyJaxbRootModel(delegateProperties(myMapDoncho));

        JAXBContext context = JAXBContext.newInstance(MyJaxbRootModel.class);
        Marshaller mar= context.createMarshaller();
        mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        mar.marshal(model, new File(System.getProperty("user.home") + "/Desktop/testMap.xml"));
    }


    private final static void fillMaps(Map<String, Object> map)
    {

        for(int i = 0; i<=5; i++)
        {
            Random r = new Random();
            int ran = r.nextInt(500);

            map.put(Integer.toString(ran), getSaltString());
        }
    }


    private final static void fillMapsInner(Map<String, Object> map, Map<String, Object> map1)
    {
        fillMaps(map1);
        for(int i = 0; i<=5; i++)
        {
            Random r = new Random();
            int ran = r.nextInt(500);

            map.put(Integer.toString(ran), map1);
        }
    }


    private final static String getSaltString()
    {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 7) // length of the random string.
        {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }


    private final static List<AbstractLinkedWrapper> delegateProperties(Map<String, Object> map)
    {
        List<AbstractLinkedWrapper> wrapper = new LinkedList<AbstractLinkedWrapper>();

        for(Map.Entry<String, Object> entry : map.entrySet())
        {
            if(entry.getValue() instanceof String)
            {
                wrapper.add(new LinkedWrapperClass(entry.getKey(), (String) entry.getValue()));
            }
            else if(entry.getValue() instanceof Map)
            {
                wrapper.add(new LinkedWrapperWrapperClass(entry.getKey(), "MAP", delegateProperties((Map<String, Object>) entry.getValue())));
            }
        }

        return wrapper;

    }

}



