/*
 * LinkedWrapperWrapperClass.java
 *
 * created at Nov 22, 2019 by d.balamdzhiev <d.balamdzhiev@seeburger.com>
 *
 * Copyright (c) SEEBURGER AG, Germany. All Rights Reserved.
 */
package com.doncho.jaxb.casus;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlTransient;

public class LinkedWrapperWrapperClass extends AbstractLinkedWrapper
{

    @XmlAttribute(name="key")
    private String key;

    @XmlAttribute(name = "value")
    private String value;

    @XmlElements({@XmlElement(name = "property", type = LinkedWrapperClass.class),
                  @XmlElement(name = "property", type = LinkedWrapperWrapperClass.class)})
    private List<AbstractLinkedWrapper> myMap = null;

    public LinkedWrapperWrapperClass() {
        // TODO Auto-generated constructor stub
    }


    public LinkedWrapperWrapperClass(String key, String value, List<AbstractLinkedWrapper> myMap) {

        this.key = key;
        this.value = value;
        this.myMap = myMap;
    }

    @XmlTransient
    public List<AbstractLinkedWrapper> getMyMap() {
        return myMap;
    }

    public void setMyMap(List<AbstractLinkedWrapper> myMap) {
        this.myMap = myMap;
    }

    @XmlTransient
    public String getKey() {
        return key;
    }


    public void setKey(String key) {
        this.key = key;
    }

    @XmlTransient
    public String getValue() {
        return value;
    }


    public void setValue(String value) {
        this.value = value;
    }

}



