/*
 * MainTesseract.java
 *
 * created at Dec 9, 2019 by d.balamdzhiev <d.balamdzhiev@seeburger.com>
 *
 * Copyright (c) SEEBURGER AG, Germany. All Rights Reserved.
 */
package com.doncho.tesseract.example;

import java.io.File;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

public class MainTesseract
{

    private static final String TESS_DATA_ROOT = "C:\\Users\\d.balamdzhiev\\Desktop\\Tess4J\\tessdata";
    private static final String FILE_PATH_ROOT = "C:\\Users\\d.balamdzhiev\\Desktop\\tessData\\";

    public static void main(String[] args)
    {
        System.out.println("Tesseract is here!");

        Tesseract tess = new Tesseract();

        try
        {
            tess.setDatapath(TESS_DATA_ROOT);
            tess.setLanguage("bul");
            String outputText = tess.doOCR(new File(FILE_PATH_ROOT + "2019-12-09 10_58_02- Gong.b.jpg"));

            System.out.println("The result is: \n" + outputText);
        }
        catch (TesseractException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}



